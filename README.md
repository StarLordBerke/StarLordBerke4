<h1 align="center">Merhaba 👋, Ben Berke Mert Öztürk </h1>
 <a target="_blank"><img align="left" height="400" width="400" alt="𝙶𝙸𝙵" src="https://github.com/JayantGoel001/JayantGoel001/blob/master/GIF/github.gif"></a>
<br/>

### Web Tasarım , Web Geliştirme, Grafik Tasarım , SEO , Yazarlık , Wordpress/Blogger , İçerik Yöneticisi

### 🔭 Benim Hakkımda

- Merhabalar ben Berke Mert Öztürk DSC Ankara Hacı Bayram Veli Üniversitesi ekibinde yer almaktayım. Yönetim Bilişim Sitemleri ve Bilgisayar Programcılığı öğrencisiyim. Ayrıca Grafik Tasarımcısı olarakta çalışmalar yapmaktayım. Adobe yazılımların büyük bir bölümüne hakim bir kullanıcısıyım. Tasarım ve konsept sanatlarına çok ilgi duyuyorum. Başlangıç seviyesindeyim ama kendimi zamanla daha fazla geliştirebilirim çünkü çok istekliyim.

- Ayrıca; yazılıma da bayılıyorum. Boş vakitlerimde "full stack developer" olabilmek için sürekli yazılım, programlama ve tasarım konsun da kendimi geliştirmeye çalışıyorum. Böylece çok istediğim "web developer" olma hedefime daha da yakınlaşacağımı düşünüyorum.


- Bilgisayar oyunlarını ve bilgisayar sistemlerini araştırmayı seviyorum. Hayalim, tasarım ve yazılımı birleştirerek ortaya güzel projeler çıkarabilmek. Genelde sosyal medyayı, aktif kullanmaktayım. Tasarım, yazılım ve teknolojiye araştırmayı yeni bilgiler öğrenmeyi çok seviyorum. Ayrıca sıfır ve ikinci el bilgisayar toplamak isteyenlere yardımcı oluyorum.

### :computer:Eğitim / Kariyer
- :computer:İstanbul Üniversitesi "Yönetim Bilişim Sistemleri" bölümü 2. sınıf öğrencisiyim. 
- :computer:Atatürk Üniversitesi "Bilgisayar Programcılığı" bölümü 2. sınıf öğrencisiyim.
- :computer:Anadolu Üniversitesi "Web Tasarımı ve Kodlama" bölümünü başarı ile tamamladım.
- :computer:Ankara Hacı Bayram Veli Üniversitesi "Turizm İşletmeciliği" bölümü 4.snıf öğrencisiyim.
- :computer:DSC Ankara Hacı Bayram Veli Üniversitesi Core Team ekibinde yer almaktayım. 
- :computer:Full Stack Developer ve Graphic Design olmak için çabalıyorum. 
- :computer:Kendimi her alanda geliştirmeye özen gösteriyorum.
- :computer:Yazılım ve Tasarım üzerine youtube ve udemy platformları için eğitim setleri hazırlamayı hedefliyorum.


### 🔭 Kendimi Geliştirme Adına Yaptıklarım!
- 🔭 Web projeleri yapmayı seviyorum. Çünkü interneti seviyorum! 
- 🔭 Şu anda yazılım tarafında "HTML, CSS, JavaScript ,Php, SQL, Python" gibi dilerde uzmanlaşmaya çalışıyorum.
- 🔭 Tasarım tarafında ise; "Adobe photoshop, Adobe İllustrator, Adobe İndesing ve Canva" gibi programlarda ustalaşmaya çalışıyorum.
- 🔭 Ayrıca yazılımın yanında web sitesi yapmayı sağlayan "wordpress, blogger, wix, weebly" gibi platformları kullanarak web sitesi projeleri yapıyorum.
- 🔭 Sektör tecrübesi kazanmak için bionluk ve fiverr gibi platformlarda web design / graphic design üzerine iş yapmaktayım.
- 🔭 Son olarak youtube kanalım ve ilerde udemy platformu için hazırlayacağım eğitim setleri için kendimi sürekli geliştirmeye çalışıyorum.

### 🎧 Hobilerim!
- 🎧 Eğlenceli gerçek: Müzik dinlemeyi seviyorum.
- 🎧 Bol bol Fantastik ve Bilim Kurgu temalı kitap okumayı seviyorum.
- 🎧 Sabah yürüyüşü yapmayı severim.
- 🎧 Yüzme sporu vazgeçilmez eğlencelerimden biri.
- 🎧 Marvel/DC evrenlerinin hastasıyım bu yüzden çizgi roman okumayı seviyorum. Hayal güvümün gelişmesini sağlıyor.
- 🎧 Resim yapmaktan büyük keyif alıyorum. Özellikle karakter tasarımı üzerine çalışmaktan.
- 🎧 Boş zamanlarımda film ve dizi izlemeyi severim. Favori filmim "Lord of the Rings" favori dizilerim ise; "Mr.Robot ve Supernatural" dır.

### 📫 Bana Ulaşmak İsterseniz!
- 📫 Bana ulaşmak isterseniz **berkemertozturk1997@gmail.com** adresini kullanabilirsiniz. 
- 📫 Sosyal Medya üzerinden bana ulaşmak isterseniz **https://linktr.ee/StarLordBerke** adresini kullanabilirsiniz.
- 📫 Web üzerinden bana ulaşmak isterseniz **https://berkemertozturk97.blogspot.com/** web sitemide kullanabilirsiniz.

### 🤝 Ayrıca Youtube Kanalımı Ziyaret Edebilirsiniz!
- 🤝 Ayrıca yazılım, tasarım, teknoloji ve eğitim üzerine içerik ürettiğim bir youtube kanalım var. Kanalımın adı "Game Mega" **https://www.youtube.com/c/GameMega** takip etmeyi unutmayın.

### 💪 Sevdiğim Sözler!
- 💪 Fedakarlık yoksa; zaferde yoktur.

***:computer:	DON'T REPEAT YOURSELF (KENDİNİ TEKRAR ETME)***
<br>

***Bölümüm Bilgisayar Mühendisliği olmasada ideallerimden asla vazgeçmeden çabalamaya devam edeceğim.:medal_military:***


<p align="center">
  <img src= "https://gpvc.arturio.dev/berkcangumusisik" alt="𝚙𝚛𝚘𝚏𝚒𝚕𝚎 𝚟𝚒𝚎𝚠𝚜"> •  
  <img alt="𝙶𝚒𝚝𝙷𝚞𝚋 𝚏𝚘𝚕𝚕𝚘𝚠𝚎𝚛𝚜" src="https://img.shields.io/github/followers/berkcangumusisik?label=Followers&style=social"> •   
  <img src="https://img.shields.io/github/stars/berkcangumusisik?label=Stars" alt="𝚃𝚘𝚝𝚊𝚕 𝚂𝚝𝚊𝚛𝚜">
</p>

<h1 align="center"> 💻Berke Mert Öztürk</h1>
<p align="center">
<a href="https://github.com/StarLordBerke4">
<img height="150em" src="https://github-readme-stats.vercel.app/api?username=StarLordBerke4&show_icons=true&theme=react&include_all_commits=true&count_private=true"/> 
 <img height="110em" src="https://user-images.githubusercontent.com/74311713/129813126-5c620ff2-cc3b-47a2-b419-974708ceb5fe.png"/>
<img height="160em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=berkcangumusisik&layout=compact&langs_count=16&theme=react"/>
 </div>
</p>

![𝚐𝚒𝚝𝚑𝚞𝚋 𝚐𝚛𝚊𝚙𝚑](https://activity-graph.herokuapp.com/graph?username=StarLordBerke4&theme=react-dark&hide_border=true&area=true)

<!-- Don't Run Contribution Graph(Generate Snake) Action on your default Branch-->
![𝙶𝚒𝚝𝚑𝚞𝚋 𝙲𝚘𝚗𝚝𝚛𝚒𝚋𝚞𝚝𝚒𝚘𝚗 𝙶𝚛𝚊𝚙𝚑](https://github.com/JayantGoel001/JayantGoel001/blob/main/github-contribution-grid-snake.svg)
<!-- Don't Run Contribution Graph(Generate Snake) Action on your default Branch -->

 <h2 align="center">🤝Sosyal Medya Hesaplarım </h2>
<p align="left">
<a href="https://www.linkedin.com/in/starlordberke/" target="blank"><img align="center" src="https://velanovascular.com/wp-content/uploads/2020/06/LinkedIn.png" height="30" width="30" /></a>
<a href="https://www.instagram.com/starlordberke/" target="blank"><img align="center" src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/Instagram_logo_2016.svg/1200px-Instagram_logo_2016.svg.png"  height="30" width="30" /></a>
<a href="https://starlordberke.medium.com/" target="blank"><img align="center" src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/medium.svg" height="30" width="40" />
</a>
</p>

<h2 align="center">⚡ Languages & Frameworks</h2>

<p align="center">
  
<code><img height="40" width="40" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/csharp_original_logo_icon_146578.png"></code>
<code><img height="40" width="40" src="https://cdn.iconscout.com/icon/free/png-256/java-60-1174953.png"></code>
<code><img height="40" width="40" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/python/python.png"></code>
<code><img height="40" width="40" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/javascript/javascript.png"></code>
<code><img height="40" width="40" src="https://cdn.iconscout.com/icon/free/png-256/css-131-722685.png"></code>
<code><img height="40" width="40" src="https://www.flaticon.com/svg/static/icons/svg/1216/1216733.svg"></code>
</p>

<h2 align="center">⚡ Database (Veri Tabanı) </h2>

<p align="center">
  
<code><img height="40" width="40" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Postgresql_elephant.svg/1200px-Postgresql_elephant.svg.png"></code>
<code><img height="40" width="40" src="https://img.icons8.com/color/480/microsoft-sql-server.png"></code>

</p>

<h2 align="center">👩‍💻 Kullandığım IDE Çeşitleri</h2>

<p align="center">
  
<code><img height="40" width="40" src="https://static.wikia.nocookie.net/logopedia/images/e/e4/Visual_Studio_2013_Logo.svg/revision/latest/scale-to-width-down/250?cb=20191221122625"></code>
<code><img height="40" width="40" src="https://img.utdstc.com/icon/ebd/c75/ebdc759e8c0dd0f603ea13620f6f2ff5221bc73ac9a823e9356ca7e09b90488a:200"></code>
<code><img height="40" width="40" src="https://brandslogos.com/wp-content/uploads/images/eclipse-logo-vector.svg"></code>

</p>
<h2 align="center">👩‍🖍📐 Graphic Design (Grafik Tasarım) </h2>

<p align="center">
  
<code><img height="40" width="40" src="https://image.flaticon.com/icons/png/512/552/552222.png"></code>
<code><img height="40" width="40" src="https://image.flaticon.com/icons/png/512/552/552220.png"></code>
<code><img height="40" width="40" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQIbMt0PbIuYZFwggnJtEI2ROZ9IuOwWjMCZBE6DIlHcYD1Lg-mX5az3Nd7uHJHIfUyztY&usqp=CAU"></code>

</p>

<p>
  
  ### 📕 Son Blog Yazıları

<!-- BLOG-POST-LIST:START -->
- [Microsoft Office'in Windows 11 Sürümü Nasıl Olacak?](https://teknomega4.blogspot.com/2021/07/microsoft-officein-windows-11-surumunde.html)
- [HTML Dersleri Bölüm 2: HTML Giriş](https://sibermega4.blogspot.com/2021/08/html-dersleri-bolum-2-html-giris_23.html)
- [HTML Dersleri Bölüm 1: HTML Tarihçesi](https://sibermega4.blogspot.com/2021/08/html-dersleri-bolum-1-html-tarihcesi_94.html)
- [Samsung Galaxy A21S İncelemesi!](https://teknomega4.blogspot.com/2021/08/samsung-galaxy-a21s-incelemesi.html)
- [FPS Oyunları için En İyi Nvidia Performans Ayarları Nasıl Yapılır?](https://teknomega4.blogspot.com/2021/07/fps-oyunlar-icin-en-iyi-nvidia.html)
<!-- BLOG-POST-LIST:END -->

➡️ [daha fazla blog yazısı...](https://teknomega4.blogspot.com/)
  
  </p>

<p>
  
### 📺 En Yeni YouTube Videoları 

<!-- YOUTUBE:START -->
- [Blogger Eğitim Seti 2021](https://www.youtube.com/playlist?list=PL2zimenUN34ZkvmZCPs0jmwzpP3l4Wk4w)
- [Steam Profili Nasıl Özelleştirilir? | Hareketli Ekran Görüntüsü Vitrini Nasıl Tasarlanır?](https://youtu.be/VO19OjXq6fk)
- [YouTube ABONE OL Animasyonu Nasıl Yapılır? | CAMTASİA & CANVA](https://youtu.be/apQLeYDJzW8)
- [Youtube Kanal Resmi (Banner) Nasıl Yapılır?](https://youtu.be/Xx-X0Ln7DkA)
<!-- YOUTUBE:END -->

➡️ [daha fazla video...](https://www.youtube.com/c/GameMega)
   
</p>

![footer](https://github.com/JayantGoel001/JayantGoel001/blob/master/PNG/footer.png)
